package controleurs;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class ControleurCaisseCine {
    
  int     nbAdultes     = 0;
  int     nbEtudiants   = 0;    
  int     nbEnfants     = 0;
  boolean promotion     = false;
    
  float   montantSoiree = 0;
  float   prixMoyen     = 0;
      
  public void calculer(){
  
      float total    = 7f * nbAdultes + 5.5f * nbEtudiants +4f * nbEnfants;
       
      if( promotion ) { total=total*0.8f;}
       
      int   nbPlaces = nbEtudiants+nbEnfants+nbAdultes;
 
      float prixMoyenP;       
      if(nbPlaces>0){prixMoyenP=total/nbPlaces;}else{prixMoyenP=0f;}
       
      setMontantSoiree(total);
      setPrixMoyen(prixMoyenP);
  }
      
  public static final String PROP_NBADULTES = "nbAdultes";
  
  public int getNbAdultes() {
      return nbAdultes;
  }
  
  public void setNbAdultes(int nbAdultes) {
      int oldNbAdultes = this.nbAdultes;
      this.nbAdultes = nbAdultes;
      propertyChangeSupport.firePropertyChange(PROP_NBADULTES, oldNbAdultes, nbAdultes);
  }
  private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  public void addPropertyChangeListener(PropertyChangeListener listener) {
      propertyChangeSupport.addPropertyChangeListener(listener);
  }
  
  public void removePropertyChangeListener(PropertyChangeListener listener) {
      propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  
    public static final String PROP_NBETUDIANTS = "nbEtudiants";

    public int getNbEtudiants() {
        return nbEtudiants;
    }

    public void setNbEtudiants(int nbEtudiants) {
        int oldNbEtudiants = this.nbEtudiants;
        this.nbEtudiants = nbEtudiants;
        propertyChangeSupport.firePropertyChange(PROP_NBETUDIANTS, oldNbEtudiants, nbEtudiants);
    }
  
    
    public static final String PROP_NBENFANTS = "nbEnfants";

    public int getNbEnfants() {
        return nbEnfants;
    }

    public void setNbEnfants(int nbEnfants) {
        int oldNbEnfants = this.nbEnfants;
        this.nbEnfants = nbEnfants;
        propertyChangeSupport.firePropertyChange(PROP_NBENFANTS, oldNbEnfants, nbEnfants);
    }

    public static final String PROP_PROMOTION = "promotion";

    public boolean isPromotion() {
        return promotion;
    }

    public void setPromotion(boolean promotion) {
        boolean oldPromotion = this.promotion;
        this.promotion = promotion;
        propertyChangeSupport.firePropertyChange(PROP_PROMOTION, oldPromotion, promotion);
    }

    
    public static final String PROP_MONTANTSOIREE = "montantSoiree";

    public float getMontantSoiree() {
        return montantSoiree;
    }

    public void setMontantSoiree(float montantSoiree) {
        float oldMontantSoiree = this.montantSoiree;
        this.montantSoiree = montantSoiree;
        propertyChangeSupport.firePropertyChange(PROP_MONTANTSOIREE, oldMontantSoiree, montantSoiree);
    }
    
        
    public static final String PROP_PRIXMOYEN = "prixMoyen";

    public float getPrixMoyen() {
        return prixMoyen;
    }

    public void setPrixMoyen(float prixMoyen) {
        float oldPrixMoyen = this.prixMoyen;
        this.prixMoyen = prixMoyen;
        propertyChangeSupport.firePropertyChange(PROP_PRIXMOYEN, oldPrixMoyen, prixMoyen);
    }

    
 
  
}



